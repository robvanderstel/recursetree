﻿using System;
using System.Collections.Generic;

namespace TestConsole
{
   class Element
   {
      public int depth;
      public string name;
      public bool visible;
      public bool accesible;
   }

   class ResultElement
   {
      public Element element;
      public List<ResultElement> resultElements = null;

      /*
       * Method that recursively updates the visibility of the element
       */
      public bool SetVisibility(string filter)
      {
         element.visible = element.name.Contains(filter);

         foreach (ResultElement resultElement in resultElements)
         {
            if (resultElement.SetVisibility(filter))
            {
               element.visible = true;
            }
         }
         return element.visible;
      }

      /*
       * Method that recursively shows the element using depth indentation
       */
      public void Show(int depth, bool visibleOnly)
      {
         if (!visibleOnly || element.visible)
         {
            for (int i = 0; i < 3 * depth; i++)
            {
               Console.Write('-');
            }

            Console.WriteLine(" Depth {0} {1} ({2})",
                                 element.depth,
                                 element.name,
                                 element.visible ? "visible" : "hidden");
         }

         foreach (ResultElement resultElement in resultElements)
         {
            resultElement.Show(depth+1, visibleOnly);
         }
      }
   }

   class MainClass
   {
      public static void Main(string[] args)
      {
         /*
          * Initialisation of a flat list based on a fixed list of depths
          * Succesive elements will be named with an index for identification
          */
         List<Element> inputElements = new List<Element>();
         int idx = 0;

       int[] depths = { 0, 1, 2, 2, 1, 2, 0, 1, 2, 3, 3, 3, 2, 3, 1, 2, 0, 1, 0, 1, 2, 3, 0 };
//         int[] depths = { 0, 1, 2, 1 };

         for (int i = 0; i < depths.Length; i++)
         {
            Element element = new Element();
            element.depth = depths[i];
            element.name = String.Format("Element {0}", i + 1);
            element.visible = false;
            element.accesible = element.name.Contains("0");
//            element.accesible = element.name.Contains("1");
            inputElements.Add(element);
         }

         /*
          * Just show how the flat input list now looks like
          */
         foreach (Element element in inputElements)
         {
            Console.WriteLine("Depth {0} {1} {2}", element.depth, element.name, element.accesible);
         }

         /*
          * Actuall call to create the tree list from the flat list
          * Tree list node contains an element and a list of child elements
          */
         List<ResultElement> resultElements = new List<ResultElement>();

         CreateTree(inputElements, ref idx, 0, resultElements);

         /*
          * Tree has been created. Now we call a visibility method that using the filter string to
          * select elements. If an element is visible then it is updated as well as all its parents.
          */
         foreach (ResultElement element in resultElements)
         {
            element.SetVisibility("2");
         }

         /*
          * Simple output procedure to show the resulting treelist using indents (all elements)
          */
         Console.WriteLine("--------------------");

         foreach (ResultElement resultElement in resultElements)
         {
            resultElement.Show(0, false);
         }

         /*
          * Simple output procedure to show the resulting treelist using indents (only visible elements)
          */
         Console.WriteLine("--------------------");

         foreach (ResultElement resultElement in resultElements)
         {
            resultElement.Show(0, true);
         }
      }

      public static void AddAccesibleElement(List<ResultElement> resultElements, ref ResultElement resultElement, bool bAccesible, ref bool bOverallAccesible)
      {
         if (resultElement == null)
            return;

         if (bAccesible)
         {
            resultElements.Add(resultElement);
            bOverallAccesible = true;
         }

         resultElement = null;
      }

      // Important that 'idx' is a ref parameter. It must keep its value when returning from the CreateTree method
      public static bool CreateTree(List<Element> elements, ref int idx, int depth, List<ResultElement> resultElements)
      {
         // TODO: extra, verify that recursion will only be done when depth increments by 1 otherwise there is an error

         // Keep track of the last created 'parent' for referncing when creating and adding children
         ResultElement currentResultElement = null;
         bool bAccesible = false;
         bool bOverallAccesible = false;

         // Iterate through the elements of the flat input list
         while (idx < elements.Count)
         {
            // if element depth is larger then recurse for children
            if (elements[idx].depth > depth)
            {
               if (CreateTree(elements, ref idx, depth + 1, currentResultElement.resultElements))
               {
                  bAccesible = true;
               }
               // do not return here !
            }
            else
            {
               // here add the current element if it is accesible or one of its children is accesible
               AddAccesibleElement(resultElements, ref currentResultElement, bAccesible, ref bOverallAccesible);

               // If the element depth is equal then create and populate a new result element
               if (elements[idx].depth == depth)
               {
                  currentResultElement = new ResultElement();
                  currentResultElement.element = elements[idx];
                  currentResultElement.resultElements = new List<ResultElement>();
                  bAccesible = currentResultElement.element.accesible;

                  idx++;   // Select next flat input element
                  // do not return here !
               }

               // if element depth is smaller then return to parent
               else
               {
                  return bOverallAccesible;
               }
            }
         }

         // here add the current element if it is accesible or one of its children is accesible
         AddAccesibleElement(resultElements, ref currentResultElement, bAccesible, ref bOverallAccesible);

         return bOverallAccesible;
      }
   }
}
